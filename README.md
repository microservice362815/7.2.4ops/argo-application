# 「7.2.4 GitOpsの実装」のArgo CD実装

7章 Argo CDの実装でGitOpsを実現するためのマニフェストです。

## 各ファイルの説明

### `gitlab-https-repo-secret.yaml`

GitLabに管理されているデプロイマニフェスト用Gitリポジトリへのアクセス権限を、KubernetesクラスタにデプロイされたArgo CDへ付与するための `Secret` リソースです。
なお、サンプルのためGit上に `Secret` リソースとして管理していますが、ご自身が管理される際は、 [External Secrets](https://external-secrets.io/)、[Hashicorp Vault](https://www.vaultproject.io/)、[Bank Vault](https://bank-vaults.dev/)などの**シークレットマネージャを利用し、より安全に管理することを推奨します**。

#### `gitlab-https-repo-secret.yaml` の設定

`< >` の箇所を環境に合わせて修正してください。

|プロパティ|説明|設定例|
|--------|----|----|
|`stringData.url`|同期対象のGitLabリポジトリのURL|`https://gitlab.com/gihyo-ms-dev-book/handson/sec7/7.2.4.gitops/deployment.git`|
|`stringData.password`|デプロイトークン| - (GitLabにて自動生成)|
|`stringData.username`|デプロイトークン生成時のUsername|`argocd`|

#### `gitlab-https-repo-secret.yaml` の適用

以下のコマンドを実行し、 Kubernetesクラスタへデプロイします。

```bash
$ kubectl apply -f gitlab-https-repo-secret.yaml
// omit
```

### `application.yaml`

Argo CDにて、デプロイマニフェストを同期設定するための `Application` リソースを管理するデプロイマニフェストです。

#### `application.yaml` の設定

`< >` の箇所を環境に合わせて修正してください。

|プロパティ|説明|設定例|
|--------|----|----|
|`spec.source.repoURL`|同期対象のGitLabリポジトリのURL|`https://gitlab.com/gihyo-ms-dev-book/handson/sec7/7.2.4.gitops/deployment.git`|

#### `application.yaml` の適用

以下のコマンドを実行し、 Kubernetesクラスタへデプロイします。

```bash
$ kubectl apply -f application.yaml
// omit
```
